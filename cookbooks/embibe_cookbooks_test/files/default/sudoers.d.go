# file /etc/sudoers.d/go
# preferably edit using visudo -f <file>

# allow the go user to run some commands using sudo. Need to specify full path $
Cmnd_Alias GO_SUDO_CMDS = /usr/local/bin/bundle, /bin/cp

# don't require a tty for running sudo
Defaults!GO_SUDO_CMDS !requiretty

# don't require password
go ALL=(ALL) NOPASSWD: GO_SUDO_CMDS