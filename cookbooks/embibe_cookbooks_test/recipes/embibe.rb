execute "sudo apt-get update" do
	command "sudo apt-get update"
end


package "nginx" do
action :install
end

service "nginx" do
  supports :start => true, :reload => true
  action :start
end

git "/usr/share/nginx/www/blog" do
  repository "https://github.com/hsachdevah/site.git"
  revision "master"
  action :checkout
end