template "/etc/default/go-agent" do
	source "goagent.erb"
	mode 0644
  	owner "go"
  	group "go"
end

cookbook_file "/etc/sudoers.d/go" do
	source "sudoers.d.go"
	mode 0440
  	owner "root"
  	group "root"
end

service "go-agent" do
	action :restart
end